var abi = [{"constant":true,"inputs":[{"name":"eventID","type":"uint256"},{"name":"idx","type":"uint256"}],"name":"getEventPlacedBetByIndex","outputs":[{"name":"id","type":"uint256"},{"name":"amount","type":"uint256"},{"name":"betType","type":"uint256"},{"name":"values","type":"uint256[2]"},{"name":"odd","type":"uint256"},{"name":"user","type":"address"},{"name":"number","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"eventID","type":"uint256"}],"name":"getEventByID","outputs":[{"name":"id","type":"uint256"},{"name":"ts","type":"uint256"},{"name":"home","type":"string"},{"name":"away","type":"string"},{"name":"sport","type":"string"},{"name":"betTypes","type":"uint256[]"},{"name":"closedBetTypes","type":"uint256[]"},{"name":"active","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"value","type":"uint256"}],"name":"withdraw","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getEventIDs","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"eventID","type":"uint256"},{"name":"betType","type":"uint256"}],"name":"closeBetType","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"eventID","type":"uint256"},{"name":"betID","type":"uint256"}],"name":"placeBet","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":true,"inputs":[{"name":"eventID","type":"uint256"}],"name":"getEventPlacedBetsAmount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"eventID","type":"uint256"},{"name":"betType","type":"uint256"},{"name":"values","type":"uint256[2]"}],"name":"announceResult","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"eventID","type":"uint256"},{"name":"betID","type":"uint256"}],"name":"getEventBetByID","outputs":[{"name":"id","type":"uint256"},{"name":"amount","type":"uint256"},{"name":"betType","type":"uint256"},{"name":"values","type":"uint256[2]"},{"name":"odd","type":"uint256"},{"name":"user","type":"address"},{"name":"number","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"eventID","type":"uint256"},{"name":"betType","type":"uint256"},{"name":"betID","type":"uint256"},{"name":"values","type":"uint256[2]"},{"name":"odd","type":"uint256"},{"name":"number","type":"uint256"}],"name":"updateBet","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"},{"name":"ts","type":"uint256"},{"name":"home","type":"string"},{"name":"away","type":"string"},{"name":"sport","type":"string"},{"name":"betTypes","type":"uint256[]"}],"name":"createEvent","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"eventID","type":"uint256"}],"name":"getEventAvailableBets","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"isCallerAdmin","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"user","type":"address"},{"indexed":true,"name":"eventID","type":"uint256"},{"indexed":false,"name":"betIndex","type":"uint256"},{"indexed":false,"name":"winnings","type":"uint256"}],"name":"BetWon","type":"event"}];


function init(){
    console.log("success")
    if (typeof web3 === 'undefined') {
		//if there is no web3 variable
		displayMessage("Error! Are you sure that you are using metamask?");
	} else {
		displayMessage("Welcome to our DAPP!");
		connectContract();
	}
}

var inst;

var address = "0xd99748782d7643b00c36a4bb296c4a099df98ff3";
var acc;

//temp fix:
//var Web3 = require("web3");
//var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

init()

function connectContract(){
	var Contract = web3.eth.contract(abi);
    inst = Contract.at(address);
	updateAccount();
}

function updateAccount(){
	//in metamask, the accounts array is of size 1 and only contains the currently selected account. The user can select a different account and so we need to update our account variable
	acc = web3.eth.accounts[0];
}

function displayMessage(message){
    console.log(message);
}

async function test() {
    let res = await getEventByID(1);
    console.log(res, res[0].toFixed());
}

function isCallerAdmin() {
    return new Promise((accept, reject) => {
        inst.isCallerAdmin.call({"from": acc}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    })
}

function getEventIDs() {
    return new Promise((accept, reject) => {
        inst.getEventIDs.call({"from": acc}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    })
}

function getEventByID(eventID) {
    return new Promise((accept, reject) => {
        inst.getEventByID.call(eventID, {"from": acc}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    })
}

function getEventBetByID(eventID, betID) {
    return new Promise((accept, reject) => {
        inst.getEventBetByID.call(eventID, betID, {"from": acc}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    });
}

function getEventPlacedBetByIndex(eventID, betIndex) {
    return new Promise((accept, reject) => {
        inst.getEventPlacedBetByIndex.call(eventID, betIndex, {"from": acc}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    });
}

function getEventAvailableBets(eventID) {
    return new Promise((accept, reject) => {
        inst.getEventAvailableBets.call(eventID, {"from": acc}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    });
}

function getEventPlacedBetsAmount(eventID) {
    return new Promise((accept, reject) => {
        inst.getEventPlacedBetsAmount.call(eventID, {"from": acc}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    });
}

async function getEvents(){
    let ids = await getEventIDs();
    objects = []

    for(let i=0; i<ids.length; i++) {
        let res = await getEventByID(ids[i]);
        objects.push(res);
    }

    return objects;
}

async function getEventAvailableBetsObjects(eventID) {
    let ids = await getEventAvailableBets(eventID);
    
    objects = []

    for(let i=0; i<ids.length; i++) {
        let res = await getEventBetByID(eventID, ids[i]);
        objects.push(res);
    }

    return objects;
}

async function getEventPlacedBetsObjects(eventID) {
    let amount = await getEventPlacedBetsAmount(eventID);
    amount = +amount.toFixed();

    objects = []

    for(let i=0; i<amount; i++) {
        let res = await getEventPlacedBetByIndex(eventID, i);
        objects.push(res);
    }

    return objects;
}

// MODIFYING FUNCTIONS

function createEvent(eventID, ts, home, away, sport, betTypes) {
    return new Promise((accept, reject) => {
        inst.createEvent(eventID, ts, home, away, sport, betTypes, {"from": acc}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    });
}

function updateBet(eventID, betType, betID, values, odd, number) {
    return new Promise((accept, reject) => {
        inst.updateBet(eventID, betType, betID, values, odd, number, {"from": acc}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    });
}

function announceResult(eventID, betType, values) {
    return new Promise((accept, reject) => {
        inst.announceResult(eventID, betType, values, {"from": acc}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    });
}

function placeBet(eventID, betID, val) {
    return new Promise((accept, reject) => {
        inst.placeBet(eventID, betID, {"from": acc, "value": val}, function(err, res){
            if(!err) {
                accept(res);
            } else {
                reject(err);
            }
        });
    });
}

//test();
