import React, { Component } from "react";
import { xml2json } from 'xml-js'
import Dropzone from 'react-dropzone'

export default class AdminPage extends Component {

    state = { xml: "", json: "" }

    onDrop(acceptedFiles, rejectedFiles) {
        
        let reader = new FileReader();

        reader.onload = (async () => {    
            this.setState({ xml: reader.result })
            this.setState({ json: xml2json(reader.result) })

            //TODO: solidity calls
            let js = JSON.parse(this.state.json);

            console.log(js, js.elements, js[0]);
            let events = js.elements[0].elements;
            console.log(events[0].name, events[1].name)

            for(let i=0; i<events.length; i++) {
                let ev = events[i];
                let attr = ev.attributes;
                let ts = (new Date(attr.EventTime).getTime()/1000);
                console.log(ts);

                let res = await createEvent(attr.ID, ts, attr.Home, attr.Away, ev.name, [1,2]) //eslint-disable-line
                console.log(res);
                res = await getEventByID(attr.ID); //eslint-disable-line
                console.log(res);

                let els = ev.elements;

                for(let j=0; j<els.length; j++) {
                    let el = els[j];
                    let at = el.attributes;
                    let betType = at.Name;
                    console.log(el);

                    for(let k=0; k<el.elements.length; k++) {
                        let bet = el.elements[k];
                        let betAt = bet.attributes;
                        console.log(betAt);
                        
                        let vals;

                        if(betAt.hasOwnProperty("Description")) {
                            if(betAt.Description.search("Goals") >=0){
                                let k = betAt.Description.split(" ")[0];
                                vals = [+k, 0];
                            } else if(betAt.Description == "Yes") {
                                vals = [1,0];
                            } else if(betAt.Description == "No") {
                                vals = [0,0];
                            } else{
                                vals = betAt.Description.split(" - ");
                            }
                        } else {
                            if (betAt.Participant == "HOME") {
                                vals = [0, 0];
                            } else if (betAt.Participant == "AWAY") {
                                vals = [1, 0];
                            }  else if (betAt.Participant == "DRAW") {
                                vals = [2, 0];
                            }
                        }

                        let betTypes = {"Match Result": 1,
                    "Correct Score Sets": 2, "Correct Score": 2, "Total Goals": 3, "Penalty in Match" : 4}

                        let odds = (+betAt.OddsDecimal)*100;
                        console.log(vals, odds);

                        console.log(attr.ID, betTypes[betType], betAt.ID, vals, odds, betAt.Number);
                        try{
                            let res = await updateBet(attr.ID, betTypes[betType], at.ID, vals, odds, betAt.Number) //eslint-disable-line
                        } catch (e) {
                            console.log(e, "Error!")
                            //k--;
                        }
                    }
                }
            }
        }).bind(this)

        acceptedFiles.forEach((file) => {
            reader.readAsText(acceptedFiles[0]);
        })

    }

    render() {
        return (
            <div className="admin-page">
                <Dropzone accept=".xml"
                    onDrop={this.onDrop.bind(this)} >
                    {({ isDragActive, isDragReject, acceptedFiles, rejectedFiles }) => {
                        if (isDragActive) {
                            return "This file is authorized";
                        }

                        if (isDragReject) {
                            return "This file is not authorized";
                        }

                        return acceptedFiles.length || rejectedFiles.length
                        ?  <p class="xml"> Accepted {acceptedFiles.length}, rejected {rejectedFiles.length} files </p>
                        :  <p class="xml"> Drop your XML </p>;
                    }}
                </Dropzone>               
            </div>
        )
    }
}