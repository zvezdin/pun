import React, { Component } from "react";
import { Input, Button, Container, Menu, Image, Dropdown, Header, Segment } from "semantic-ui-react"
import {xml2json} from 'xml-js'

export default class BetPage extends Component {

	constructor(props) {
		super(props)

		this.state = {
			fetching: false,
			fetched: false,
			sorted: false,
			events: [],
			availableBets: [],
			availableTypes: [],
			closedTypes: [],
			chosenBetID: "",
			chosenBet: [],
			chosenEventID: [],
			yourBet: {},
			yourBetID: 0,
			amount: 0,
		}

		this.getAllEvents()
	}

	betTypes = {
		1 : {
			name: "Match Results",
			bets: [],
		},
		2: {
			name: "Correct Score",
			bets: []
		},
		3: {
			name: "Total Score",
			bets: []
		},
		4: {
			name: "Penalty in Match",
			bets: []
		}
	}

	handleChange(e, {name, value}) {
		this.setState({ [name] : value })

		if (name === "amount") {
			this.state.yourBet.amount = value;
		}

	}

	parseBetTypes(raw) {
		if (raw.length > 0){
			let rawBetTypes = []
			
			raw.forEach((betType) => {
				rawBetTypes.push(betType.toFixed())
			})

			let betTypes = []

			rawBetTypes.forEach((betType) => {
				betTypes.push(this.betTypes[betType])
			})
	
			return betTypes
		}
	}


	parseBet(raw) {
		// amount
		// betType
		// values
		// odds
		// user 
		// number
		let bet = {
			id: raw[0].toFixed(),
			amount: raw[1].toFixed(),
			betType: this.parseBetTypes([raw[2]]),
			values: [ raw[3][0].toFixed(), raw[3][1].toFixed() ],
			odds: parseInt(raw[4].toFixed()) / 100, 
			user: raw[5],
			number: raw[6].toFixed()
		}

		return bet
	}

	parseEvent(propArray) {
		//bigInt
		let id = propArray[0].toFixed()
		let ts = propArray[1].toFixed()
		
		//strings
		let home = propArray[2]
		let away = propArray[3]
		let sport = propArray[4]
		
		//bigInt
		let betTypes = this.parseBetTypes(propArray[5])
		let closedBetTypes = this.parseBetTypes(propArray[6])
		//bool
		let active = propArray[7]
		
		this.setState({
			availableTypes: betTypes,
		})

		return {
			id: id,
			timestamp: ts,
			home: home,
			away: away,
			sport: sport,
			betTypes: betTypes,
			active: active,
		}
	}

    handleClick = (e) => {
		this.state.availableTypes.forEach((type) => {
			type.bets = []
		})
		
		for (let i = 0; i < this.state.events.length; i++) {
			if (this.state.events[i].id === e.currentTarget.dataset.id ) {
				this.state.chosenBet =  this.state.events[i]
				this.state.closedTypes.push(this.state.events[i].closedBetTypes)
			}
		}
		this.setState({ chosenBetID: e.currentTarget.dataset.id })
		this.getAvailableBets(e.currentTarget.dataset.id)
	}

	async getAvailableBets(id) {
		let availableBets = await getEventAvailableBetsObjects(id); //eslint-disable-line
		let parsedBets = []

		availableBets.forEach((bet) => {
			parsedBets.push(this.parseBet(bet))
		})

		if(this.state.chosenBet.home === "Bulgaria" || this.state.chosenBet.away === "Bulgaria" ) {
			let shit = "<FootballEvent ID='5097152' EventTime='2018-11-13T19:00:00' Home='Bulgaria' Away='Germany'> <Market ID='25179210' Number='1' Name='Match Result'> <Selection ID='60475643' Number='1' OddsDecimal='2.55' Participant='HOME' /> <Selection ID='60475645' Number='2' OddsDecimal='3.83' Participant='DRAW' /> <Selection ID='60475644' Number='3' OddsDecimal='2.31' Participant='AWAY' /> </Market> <Market ID='25179211' Number='2' Name='Correct Score'> <Selection ID='60475646' Number='1' Description='0 - 0' OddsDecimal='47.48' Participant='DRAW' /> <Selection ID='60475652' Number='2' Description='1 - 1' OddsDecimal='5.42' Participant='DRAW' /> <Selection ID='60475657' Number='3' Description='2 - 2' OddsDecimal='14.83' Participant='DRAW' /> <Selection ID='60475647' Number='4' Description='1 - 0' OddsDecimal='14.32' Participant='HOME' /> <Selection ID='60475648' Number='5' Description='2 - 0' OddsDecimal='11.52' Participant='HOME' /> <Selection ID='60475653' Number='6' Description='2 - 1' OddsDecimal='6.53' Participant='HOME' /> <Selection ID='60475649' Number='7' Description='3 - 0' OddsDecimal='20.84' Participant='HOME' /> <Selection ID='60475654' Number='8' Description='3 - 1' OddsDecimal='23.65' Participant='HOME' /> <Selection ID='60475650' Number='9' Description='4 - 0' OddsDecimal='100.57' Participant='HOME' /> <Selection ID='60475651' Number='10' Description='0 - 1' OddsDecimal='13.47' Participant='AWAY' /> <Selection ID='60475655' Number='11' Description='0 - 2' OddsDecimal='10.19' Participant='AWAY' /> <Selection ID='60475656' Number='12' Description='1 - 2' OddsDecimal='6.15' Participant='AWAY' /> <Selection ID='60475658' Number='13' Description='0 - 3' OddsDecimal='17.34' Participant='AWAY' /> <Selection ID='60475659' Number='14' Description='1 - 3' OddsDecimal='20.92' Participant='AWAY' /> <Selection ID='60475660' Number='15' Description='0 - 4' OddsDecimal='78.71' Participant='AWAY' /> </Market> <Market ID='25179248' Number='3' Name='Total Goals'> <Selection ID='60475768' Number='1' Description='0 Goals' OddsDecimal='48.44' /> <Selection ID='60475769' Number='2' Description='1 Goals' OddsDecimal='7.02' /> <Selection ID='60475770' Number='3' Description='2 Goals' OddsDecimal='2.72' /> <Selection ID='60475771' Number='4' Description='3 Goals' OddsDecimal='2.36' /> <Selection ID='60475772' Number='5' Description='4 Goals' OddsDecimal='5.49' /> </Market> <Market ID='25179221' Number='4' Name='Penalty in Match'> <Selection ID='60475707' Number='1' Description='Yes' OddsDecimal='22.24' /> <Selection ID='60475707' Number='2' Description='No' OddsDecimal='1.24' /> </Market> </FootballEvent>"
			let json = JSON.parse(xml2json(shit)).elements[0]
			
			console.log(json)
			parsedBets.push.apply([])
		}
			
		
		this.setState({ availableBets: parsedBets })
		this.betsByType()
	}

	async getAllEvents() {
		let rawEvents = await getEvents() //eslint-disable-line
		
		rawEvents.forEach((event) => {
			this.state.events.push(this.parseEvent(event))
		})

		this.setState({ events: this.state.events })
	}

	betItem (object, i) {
		let betType = object.betType[0]
		let newObj = {...object, betType: undefined}

		if (betType.name === "Match Results") {
			return (
				<Segment key={i} className="bet-item" data-obj={JSON.stringify(newObj)} onClick={this.addBet.bind(this)}>
					{ newObj.values[0] === newObj.values[1] ? "DRAW" : newObj.values[0] == 1 ? "WIN (Home)" : "LOSE (Home)"} { newObj.values[0] === newObj.values[1] ? "" : newObj.values[1] == 1 ? " : WIN (Away)" : " : LOSE (Away)"}
					<p>(Odds {newObj.odds})</p>
				</Segment>
			)
		} else {
			return (
				<Segment key={i} className="bet-item" data-obj={JSON.stringify(newObj)} onClick={this.addBet.bind(this)}>
					{newObj.values[0] + " : " + newObj.values[1] + " " }
					(Odds {newObj.odds})
				</Segment>
			)
		}
	}

	//Sorting
	betsByType() {

		this.state.availableBets.forEach((bet) => {
			this.state.availableTypes.forEach((type) => {
				if(bet.betType[0].name === type.name) {
					type.bets.push(bet)
				}
			})
		})
		this.setState({ solved: true })
	}


	betType(type, i) {
		return (
			<Segment key={i}>
				<Header>{type.name}</Header>
				{ 	this.state.availableTypes.includes(this.state.closedTypes[i]) ? "Closed" :
					this.state.availableTypes[this.state.availableTypes.indexOf(type)].bets !== 0 ?
					this.state.availableTypes[this.state.availableTypes.indexOf(type)].bets.map((object, i) => this.betItem(object, i)) : "No bets"	}
			</Segment>
		)
	}

	eventItem(data, i) {
		return(
			<Segment key={i} 
					 data-id={data.id} 
					 className={"event-item"}
					 onClick={this.handleClick}>
				{data.away} vs {data.home}
			</Segment>
		)
	}

	addBet(e) {
		let Bet = JSON.parse(e.currentTarget.dataset.obj)
		this.setState({ yourBet: Bet })
	}

	async submitBet() {
		let status = await placeBet(this.state.chosenBetID, this.state.yourBet.id, this.state.amount) //eslint-disable-line
	}

	renderAllEvents() {
		return (
			this.state.events.map((object, i) => this.eventItem(object, i))
		)
	}

	renderAvailableBets() {
		if (this.state.chosenBetID !== "") {
			return (
				<div className="available-bet">
					{this.state.chosenBet.betTypes.map((object, i) => {
						
						return this.betType(object, i)
					})}				
				</div>
			)
		}
	}

	renderChosenBet() {
		return (
			<div className="chosen-bets">
				<Segment>
					<b>{this.state.chosenBet.away} vs {this.state.chosenBet.home}</b>
					<p>Odds: {this.state.yourBet.odds}</p>
					<p>Amount to bet:</p>
					<Input name="amount" type="numbers" onChange={this.handleChange.bind(this)} value={this.state.amount}/>
					<br/>
					<p>Potential profit:</p>
					{this.state.amount ? Math.round(this.state.yourBet.odds * this.state.amount * 100) / 100 : "None"}
				</Segment>
				<Button color="green" onClick={this.submitBet.bind(this)}>
					Submit your bet
				</Button>
			</div>
		)
	}

    render() {		
        return(
            <div className="bet-page">
				{/* Nav bar */}			
        		<Menu borderless>
					<Container text>
						<Menu.Item>
							<Image size='mini' className="logo" src='/img/logo.png' />
						</Menu.Item>
						<Menu.Item header>Personal Unified betting Network (P.U.N.)</Menu.Item>
					</Container>
				</Menu>
				{/* Nav bar */}
				<div className="content">
					<Segment className="rear all-event-types">
						<Header attached className="titles"> All Bets/Sports </Header>
						{this.state.events.length !== 0 ? this.renderAllEvents() : <p className="no-bet"> No bet chosen </p>}
					</Segment>
					<Segment className="front all-available-bets">
						<Header attached className="titles">{this.state.chosenBet.sport ? this.state.chosenBet.sport + ">" : null} Available Bets { this.state.chosenBet.length !== 0 ? " - " + this.state.chosenBet.home + " vs " + this.state.chosenBet.away : null} </Header>
						{this.state.chosenBetID !== "" ? this.renderAvailableBets() : <p className="no-bet"> No bet chosen </p>}
					</Segment>
					<Segment className="rear bets-to-submit">
						<Header attached className="titles"> Current bets </Header>
						{this.state.chosenBetID !== "" ? this.renderChosenBet() : <p className="no-bet"> No bet chosen </p>}					
					</Segment>

				</div>
            </div>
        )
    }
}